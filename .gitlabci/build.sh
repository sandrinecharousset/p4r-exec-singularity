#!/bin/bash

set -o errexit
set -o nounset

################################################################################
### Recipe File ################################################################
################################################################################


recipe=$1
sandbox=$2
mpidist=$3

echo ""
echo "Image Recipe: ${recipe}"

################################################################################
### Build! #####################################################################
################################################################################

# Continue if the image recipe is found
if [ -f "$recipe" ]; then

    if [ -z "$sandbox" ]; then
	image="${recipe%_mpi.*}_$mpidist.sif"
	# Write configuration file
	cat > ${recipe%_mpi.*}_conf.sh <<EOF
	    export MPI_DIST=$mpidist
EOF
    else
	image="${recipe%.*}.imgdir"
    fi

    echo "Creating $image using $recipe..."
    singularity build $sandbox $image $recipe

    # If the image is successfully built, test it

    if [ -f "${image}" -o -d "${image}" ]; then
	echo "Image building completed"
    else
	echo "Singularity recipe ${recipe} not found!"
	exit 1
    fi
fi
